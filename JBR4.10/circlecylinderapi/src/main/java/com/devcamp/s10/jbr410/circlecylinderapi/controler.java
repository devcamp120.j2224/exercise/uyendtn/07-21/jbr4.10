package com.devcamp.s10.jbr410.circlecylinderapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.jbr410.circlecylinderapi.model.Circle;
import com.devcamp.s10.jbr410.circlecylinderapi.model.Cylinder;

@RestController
public class controler {
    @CrossOrigin
    @GetMapping("circle-area")
    public double getCircleArea(@RequestParam(value = "radius")double radius) {
        Circle newCircle = new Circle(radius);
       return newCircle.getArea();
        
    }

    @GetMapping("cylinder-volume")
    public double getCylinderVolume(@RequestParam(value = "radius")double radius, @RequestParam (value="height") double height) {
        Circle newCircle = new Circle(radius);

       return newCircle.getArea()*height;
        
    }
}
